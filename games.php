<?php
    /*
    * PAST USER ANSWERS
    */
    if ( isset($_POST['past']) ){
        $pastNb = $_POST['past'] ;
    }
    else {
        $pastNb = [] ;
    }  
    /*
    * GAME ANSWER
    */
    if ( isset($_POST['answer']) ) {
        $answer = $_POST['answer'] ; 
    }
    else {
        $answer = random_int(0, 100) ;
    }
    /*
    * TRIES
    */
    if ( isset($_POST['tries']) ) {
        $triesNb = $_POST['tries'] ;        
    }
    else {
        $triesNb = 5 ;
    }
    /*
    * LAST USER ANSWER 
    */
    if ( isset($_POST['nb-in']) && $_POST['nb-in'] != '' ) {
        $inputNb = $_POST['nb-in'] ; 
        array_push($pastNb , $inputNb) ;      
    }
    else {
        $inputNb = null ;
    }   
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Games . PHP</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <h1 class="text-warning m-5 text-center" > 💯 GAME : Find the magic number !  💯 </h1>

    <?php   
    if ( isset($_POST['nb-in']) && $_POST['nb-in'] == '' ){ ?>
        <h3 class="text-warning text-center"> Il faut rentrer un nombre 😎 </h3>

    <?php  };

    if ( isset($inputNb) ) { 
        if ( $inputNb == $answer){ 
    ?>
        <h2 class="text-warning text-center"> 🥳 YOU WIN ! 🥳 </h2>
        <h3 class="text-warning text-center m-2">La réponse était: <?php echo $inputNb ?> 🐱‍👤</h3>
    <?php   }
        else {
            $triesNb -= 1 ;
            if ($inputNb < $answer){
    ?> 
            <h3 class="text-warning text-center" ><?php echo $inputNb ?>  ? Vous êtes en dessous ... (<?php echo $triesNb ?> chances restantes)</h3>
    <?php
            }
            elseif ($inputNb > $answer){
    ?> 
            <h3 class="text-warning text-center" ><?php echo $inputNb ?> ? Vous êtes au dessus ... (<?php echo $triesNb ?> chances restantes)</h3>
    <?php
            }
        };  
        if ($triesNb == 0){
    ?> 
            <h3 class="text-warning text-center" >Vous avez perdu ... 🐐 </h3>
            <h3 class="text-warning text-center" >La réponse était: <?php echo $answer ?> 🧐 </h3>
    <?php 
        }
    }
    ?>

    <?php if ($triesNb > 0 && $answer != $inputNb){ ?>
        <div id="form-box" class="my-5 mx-auto w-50" >
            <form action="" method="POST" class="d-flex flex-column">
                <label class="text-light " for="nb-in">Entrez un nombre entre 0 et 100 !</label>
                <input type="number" name="nb-in" id="nb-in" class="text-center my-2" min="0" max="100" >
                <input type="number" name="answer" id="answer" class="d-none" value="<?php echo $answer ?>" >
                <input type="hidden" name="tries" id="tries" value="<?php echo $triesNb ?>" >

                <?php foreach ($pastNb as $key => $value) {
                    echo '
                <input type="hidden" name="past['.$key.']" id="past['.$key.']" value=' . $value .' >
                        ' ;
                }  ?> 
                <input type="submit" value="Pouet !">
            </form>
        </div>
    <?php } ?>
    <ul id="pastNbList" class="text-">
        <?php if (isset($pastNb)){
            foreach ($pastNb as $key => $value) {
                if ($value < $answer){
                    $indice = "(Trop petit...)" ;
                }
                elseif ($value > $answer){
                    $indice = "(Trop grand...)" ;
                }
                else {
                    $indice = '(Victoire !!!)';
                }
                echo '<li class="list-group-item m-2 rounded-1 w-50 bg-dark text-warning">Essai n°'. ($key + 1) .' => '. $value .'  '.$indice.'</li>' ;
            }
        } ?>
    </ul>
    <div class="text-center" >
        <a href="http://localhost/les-bonnes-pages/games.php" class="text-center text-light">Rejouer ?</a>
    </div>
</body>
</html>