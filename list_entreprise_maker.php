<?php 
    require_once('./data_list_entreprise.php');
    
    foreach ($entreprises as $entreprise) {
        if(isset($_SESSION['user'])){
            $adminBtn = '
            <a class="btn btn-success" href="./update_entreprises_form.html?id='.$entreprise["id"].'">Mettre à jour</a>
            <a class="btn btn-danger" href="./secured_delete_entreprises.php?id='.$entreprise["id"].'">Supprimer</a>
            ';
            }
            else{
                $adminBtn = '';
            };
        echo '
        <li class="list-group-item bg-light text-dark p-2 m-1 rounded d-grid align-items-center">
            <div class="row">
                    <span class="col-12 col-sm-6 text-center text-wrap fw-bold " >'.$entreprise['name'].'</span>
                        <span class="col-12 col-sm-6 text-center text-wrap p-2" >
                            <a class="btn btn-dark" href="./detail.html?id='.$entreprise["id"].'">Voir</a>
                            '.$adminBtn .'
                        </span>
            </div>
             
        </li>
        ' ;
    }
?>