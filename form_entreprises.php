<?php  
if (isset($_POST['name']) AND !empty($_POST['name'])){
    // var_dump($_POST['name']);
    $name = $_POST['name'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}

if (isset($_POST['website']) AND !empty($_POST['website'])){
    // var_dump($_POST['website']);
    $website = $_POST['website'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}

if (isset($_POST['description']) AND !empty($_POST['description'])){
    // var_dump($_POST['description']);
    $description = $_POST['description'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}

if($_FILES['image']['name'] == ''){
    $image = 'default.jpg';
}else{
    $image = $_FILES['image']['name'];
}

$req = 'INSERT INTO `entreprises`(`name`, `description`, `website`, `image`) VALUES( :name  , :description , :website , :image )' ;
// echo $req ;


try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->bindParam(':name' , $name);
    $stmt->bindParam(':description' , $description);
    $stmt->bindParam(':website' , $website);
    $stmt->bindParam(':image' , $image);

    $stmt->execute();
    // $res = $pdo->query($req);
    // var_dump($res) ;

    if($_FILES['image']['name'] != ''){
        move_uploaded_file($_FILES['image']['tmp_name'], './public/img/'.$image);
    }

    header('Location: ./index.html');
    exit;
}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error.html');
    exit;

}

?>