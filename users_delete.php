<?php 

if ( isset($_GET['id']) && !empty($_GET['id'])){
    $idToDelete = $_GET['id'] ; 
}
else{
    header('Location: ./error.html');
    exit;
}
$req = "DELETE FROM `users` WHERE id = :id " ;

try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req , [ PDO::FETCH_ASSOC ]);
    $stmt->bindParam(':id' , $idToDelete);

    $res = $stmt->execute();

    header('Location: ./users_list.html?del=true');
    exit;
}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error.html');
    exit;

}

?>