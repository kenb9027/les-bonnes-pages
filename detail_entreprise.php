<?php 

$entreprise = [] ;

if ( isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'] ; 
}
else{
    header('Location: ./error.html');
    exit;
}
$req = "SELECT * FROM `entreprises` WHERE id= :id " ;

try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->bindParam(':id' , $id);

    $stmt->execute();
    
    $entreprise = $stmt->fetch(PDO::FETCH_ASSOC);

}catch(\PDOException $e) {
    echo 'oups !' ;
    
}catch(\Throwable $th){
    echo $th->getMessage() ; 
    
}

?>