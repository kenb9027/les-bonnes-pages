<?php 

if (isset($_POST['email']) && !empty($_POST['email'])){
    $email = $_POST['email'] ;
}
else {
    header('Location: error_form.html');
    exit;
}
if (isset($_POST['password']) && !empty($_POST['password'])){
    $password = $_POST['password'] ;
}
else {
    header('Location: error_form.html');
    exit;
}
require_once('./src/models/users.php');

$log = isUserExist($email , $password);


if ($log !== false){
    session_start();
    $idLogged = $log['id'];
    $pseudoLogged = $log['pseudo'];


    $_SESSION['user'] = ['id' => $idLogged , 'pseudo' => $pseudoLogged] ;
    // var_dump($_SESSION);die;


    header('Location: ./index.html');
    exit;  
}
else {
    header('Location: ./error_log.html');
    exit;
}


?>