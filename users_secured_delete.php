<?php  
    
    if ( isset($_GET['id']) && !empty($_GET['id'])){
        $id = $_GET['id'] ; 
    }
    else{
        header('Location: ./error.html');
        exit;
    }
    
    $req = "SELECT id , pseudo FROM `users` WHERE id=". $id ;
    
    try {
        require_once('./src/connect_bdd.php') ;
        $res = $pdo->query($req);
        foreach ($res as $value) {
            $users = $value ;
        }
        
    }catch(\PDOException $e) {
        echo 'oups !' ;
        
    }catch(\Throwable $th){
        echo $th->getMessage() ; 
        
    }
    
    $pageTitle ="Supprimer " . $users['pseudo'] . " ?";
?>

<!DOCTYPE html>
<html lang="en">
    <?php require_once('./templates/head.html')  ?>

<body>
    <?php require_once('./templates/navbar.html')  ?>

    <div class="container m-5">

        <h2 class="text-center text-light">Voulez vous vraiment supprimer l'utilisateur <span class="text-warning" ><?php echo $users['pseudo'] ?></span> ?</h2>
        <div class="alert alert-danger" role="alert">
            <h3 class="text-center text-danger m-1"> PRENEZ GARDE , cette action est irréversible ! </h3>
        </div>

        <span  class="d-flex w-100 justify-content-evenly m-2">
            <a class="btn btn-dark" href="./index.html">Retour à l'accueil</a>
            <a class="btn btn-danger" href="./users_delete.php?id=<?php echo $id?>">Je confirme !</a>
        </span>
        
    </div>

    
</body>
</html>