<?php 
$users = [] ;
$req = 'SELECT `id`, `pseudo`, `mail`, `created_on` , `updated_on` FROM `users` WHERE 1 ORDER BY created_on DESC ';

try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->execute();

    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
        array_push($users , $value);
    }

}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error.html');
    exit;
}
    foreach ($users as $user) {
        if(isset($_SESSION['user'])){
            $adminBtn = '
            <span class="col-6 col-md-6 col-lg-2 text-wrap text-center" >
            <a class="btn btn-success m-1" href="./users_update_form.html?id='.$user['id'].'">Mettre à jour</a>
         </span>
        <span class="col-6 col-md-6 col-lg-2 text-wrap text-center" >
            <a class="btn btn-danger m-1" href="./users_secured_delete.php?id='.$user['id'].'">Supprimer</a>
         </span>
            ';
            }
            else{
                $adminBtn = '';
            };
        echo '
        <li class="list-group-item bg-light text-dark m-1 rounded d-grid align-items-center">
            <span class="row g-0">
            <span class="col-6 col-md-3 col-lg-2 text-wrap text-center" >'.$user['pseudo'].'</span>
            <span class="col-6 col-md-3 col-lg-2 text-wrap text-center" >'.$user['mail'].'</span>
                <span class="col-12 col-md-6 col-lg-4 text-center text-wrap" >Inscrit le : '.$user['created_on'].'</span>
                 '. $adminBtn .'
            </span>    
        </li>
        ' ;
    }
?>