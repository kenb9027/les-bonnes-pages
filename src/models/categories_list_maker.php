<?php 
$categories = [] ;
$req = 'SELECT `id`, `name`, `created_on`, `updated_on` FROM `categories` WHERE 1 ORDER BY updated_on ';

try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->execute();

    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
        array_push($categories , $value);
    }

}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error.html');
    exit;
}
    foreach ($categories as $category) {


        $randColor = rand(0 , 80);
        if ($randColor <= 10){
            $color = 'info' ;
            $text = 'dark' ;
        }
        if ($randColor > 10 && $randColor <= 20){
            $color = 'success' ;
            $text = 'light' ;
        }
        if ($randColor > 20 && $randColor <= 30){
            $color = 'danger' ;
            $text = 'light' ;
        }
        if ($randColor > 30 && $randColor <= 40){
            $color = 'warning' ;
            $text = 'dark' ;
        }
        if ($randColor > 40 && $randColor <= 50){
            $color = 'dark' ;
            $text = 'light' ;
        }
        if ($randColor > 50 && $randColor <= 60){
            $color = 'light' ;
            $text = 'dark' ;
        }
        if ($randColor > 60 && $randColor <= 70){
            $color = 'secondary' ;
            $text = 'light' ;
        }
        if ($randColor > 70 && $randColor <= 80){
            $color = 'primary' ;
            $text = 'light' ;
        }



        echo '
        <span class="fw-bold p-4 m-4 rounded-pill bg-'.$color.' text-'.$text.'"><a href="./category_delete_view.html?id='.$category['id'].'" class=" text-decoration-none text-'.$text.'" >'.$category['name'].'</a></span>
        ' ;
        
    }
?>