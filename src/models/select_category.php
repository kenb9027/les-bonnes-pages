<?php
function getCategory($id){

    $req = 'SELECT `id`, `name`, `created_on`, `updated_on` FROM `categories` WHERE id=' . $id;
    
    try {
        require_once('../connect_bdd.php') ;
    
        $stmt = $pdo->prepare($req);
        $stmt->execute();
    
        $categorie = $stmt->fetch(PDO::FETCH_ASSOC);
        return $categorie ;
    
    }catch(\PDOException $e) {
        echo 'oups !' ;
        header('Location: ../error_form.html');
        exit;
    }catch(\Throwable $th){
        echo 'mince...' ; 
        header('Location: ../error_form.html');
    }
}