<?php 

if (isset($_POST['entreprise']) AND !empty($_POST['entreprise'])){
    $entrepriseId = $_POST['entreprise'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}
if (isset($_POST['categorie']) AND !empty($_POST['categorie'])){
    $categorieId = $_POST['categorie'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}


$req = 'INSERT INTO `entreprises_categories`( `id_entreprise`, `id_categorie`) VALUES ( :identreprise , :idcategorie )' ;

try {
    require_once('../connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->bindParam(':identreprise', $entrepriseId);
    $stmt->bindParam(':idcategorie', $categorieId);
    $stmt->execute();

    header('Location: ../../index.html');
    exit;

}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ../../error_form.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ../../error_form.html');
}
