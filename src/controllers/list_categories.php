<?php 
$categories = [] ;
$req = 'SELECT `id`, `name`, `created_on`, `updated_on` FROM `categories` WHERE 1 ORDER BY updated_on ';

try {
    require_once('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req);
    $stmt->execute();

    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $key => $value) {
        array_push($categories , $value);
    }

}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error.html');
    exit;
}

