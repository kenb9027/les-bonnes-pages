./<?php
if (isset($_POST['name']) AND !empty($_POST['name'])){
    $name = $_POST['name'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}

if (isset($_POST['website']) AND !empty($_POST['website'])){
    $website = $_POST['website'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}

if (isset($_POST['description']) AND !empty($_POST['description'])){
    $description = $_POST['description'] ;
}
else{
    header('Location: ./error_form.html');
    exit;
}
if (isset($_POST['id']) AND !empty($_POST['id'])){
    $id = $_POST['id'] ;
}
else{
    header('Location: error_form.html');
    exit;
}

function getOldImage($id) {

    $entreprise = [] ;
 
    $req1 = "SELECT * FROM `entreprises` WHERE id= :id " ;

    try {
        require_once('./src/connect_bdd.php') ;

        $stmt = $pdo->prepare($req1);
        $stmt->bindParam(':id' , $id);

        $stmt->execute();
        
        $entreprise = $stmt->fetch(PDO::FETCH_ASSOC);
        return $entreprise ;

    }catch(\Throwable $th){
        echo $th->getMessage() ; 
        
    }

}


if($_FILES['image']['name'] == ''){

    $entreprise = getOldImage($id) ;
    $image = $entreprise['image'] ;
}else{

    $image = $_FILES['image']['name'];
}



$req = 'UPDATE `entreprises` SET `name`= :name ,`description`= :description ,`website`= :website , `image`= :image WHERE id=' . $id ;

try {
    require('./src/connect_bdd.php') ;

    $stmt = $pdo->prepare($req , [ PDO::FETCH_ASSOC ]);
    $stmt->bindParam(':name' , $name);
    $stmt->bindParam(':description' , $description);
    $stmt->bindParam(':website' , $website);
    $stmt->bindParam(':image' , $image);

    $stmt->execute();

  

    if($_FILES['image']['name'] != ''){
        move_uploaded_file($_FILES['image']['tmp_name'], './public/img/'.$image);
       // if($image != 'default.jpg'){
            /**
             * unlink permet de supprimer un fichier
             * Nous supprimer l'ancienne image si ce n'est pas default.jpg
             */
            // unlink('./public/img/'.$image);
       // }
    }

    header('Location: detail.html?id='. $id .'&up=true');
    exit;
}catch(\PDOException $e) {
    echo 'oups !' ;
    header('Location: ./error.html');
    exit;
}catch(\Throwable $th){
    echo 'mince...' ; 
    header('Location: ./error_test.html');
    exit;

}
?>